1 introductory lecture session on Rust in general (what is it, what are its main properties, how does it fit into your organization, …)

1 in-depth lecture session on the security-relevant features. According to Frank, the type system for memory management and the concurrency system are most relevant in this space

1 lab session on using Rust in practice (90 or 180 minutes, up to you). This session would target developers with experience in low-level programming (c/c++). The contents I will leave up to you. An intro is fine, but porting a (part of) a library also seems pretty cool. It would be great if the lab would give the attendees a idea of how to get started with moving to Rust.

----

# TITLE
Introduction to Rust

# SHORT DESCRIPTION
Learn what makes Rust interesting and unique, as well as what usecases it best applies to.

# TARGET AUDIENCE
Programmers of any experience level.

# KEY TAKEAWAY
Be able to read and write introductory-level Rust code.

# ABSTRACT
Rust is a relatively new programming language that makes bold claims: that it's
blazingly fast, prevents segfaults, and guarantees thread safety. It was Stack
Overflow's most-loved programming language for the second year in a row, and
companies like Mozilla, Dropbox, Oracle and Microsoft are using Rust. What would
make these companies try something so new?

Join us for an overview of Rust and learn enough to be able to start making up
your own mind! We will talk about the things that make Rust unique and powerful,
covering both technical and non-technical factors. Among other things, the
vaunted borrow checker will be introduced and hopefully demystified. We will
also cover areas where Rust needs improvement.


what is rust
prty good error messages
enums
no null
traits
iterators
closures
async
community
cargo
rfcs
tests
fuzzing
quickcheck
C / FFI
rustdoc rustfmt clippy
some big users
 FF quantum
chucklefish
----


# TITLE
How Rust helps us make safer and more secure code

# SHORT DESCRIPTION
Learn how Rust helps prevent common programming mistakes allowed by many prevalent low-level programming languages.

# TARGET AUDIENCE
Anyone interested in details of how Rust does what it claims to. Knowledge of languages like C or C++ is useful but not required.

# KEY TAKEAWAY (100 character limit)
Understand which Rust features contribute to safer code and how to leverage them

# ABSTRACT
Rust promises to help us write better, safer code, but how exactly does it do
so? Marketing can only convince us of so much, sometimes we need to see the
details for ourselves.

The session will dive deeper into specific aspects of Rust that help make
programs safer by default. Learn how Rust's lifetimes and borrowing semantics
help prevent programming mistakes that are all-too-common in many prevalent
low-level programming languages. See how these same techiniques extend to
thornier problems like multithreading. Rust is no silver bullet, so we will also
discuss places it won't help, and we'll cover some well-known security issues
and discuss whether Rust would have mitigated the problems.

performance & compile time
if oyu are perfect, we don't help
bounds checks
overflows
what does safety mean?
unsafe
ownership
when is `char *` different from `char *`
memory management (RAII)
leaking memory -> DOS
hashmap collision -> dos
borrow semantics (mut & immut)
lifetimes
parallels to a mutex
extends to threads
aliasing & optimizations (ish)
heartbleed
cloudbleed
gotofail
ff bugs?
must_use

----


TITLE
Hands-on introduction to Rust

SHORT DESCRIPTION
Start with your first Rust program and end by integrating Rust with C.

TARGET AUDIENCE
Programmers who have experience in at least one other programming
language and an understanding of the concepts of functions, variables,
and conditionals.

KEY TAKEAWAY
Write and compile basic Rust programs and know how Rust can be integrated into larger projects.

ABSTRACT
Rust is a wonderful and powerful yet sometimes confusing language, but
everyone starts at the beginning. Getting that start alongside fellow
programmers and led by an experienced guide can make the learning
process much more smooth.

We will start from an empty file and work our way towards some basic
integration of Rust with existing C code. We will cover some of the
key ideas that make Rust different from other languages such as Rust's
build system, references and the borrow checker, and mutability.


# Prereq

Attendees should know how to use the command line. Should have Rust installed and working.


cargo new
docs
for loop
fizzbuzz
String/&str
heap/stack/refs
mutability
option / result

a tiny bit of rust and c?


----


TITLE
The title of your session

SHORT DESCRIPTION (200 character limit)
1-line summary of what your talk is about

TARGET AUDIENCE
A high-level indication of who should attend your talk

KEY TAKEAWAY (100 character limit)

The main lesson that attendees will walk away with. Delivering this take-away should be the core focus of your session.

ABSTRACT (1000 character limit)

The abstract of your talk. Ideally, the abstract is a 2-paragraph text. The first paragraph should sketch the context for the talk. The second paragraph provides an overview of the content that will be covered.



----

Example session

TITLE
The web's security model

SHORT DESCRIPTION
The web still depends on the same security model as it did 20 years ago. Even if somewhat flawed, that security model is essential for building secure applications.

TARGET AUDIENCE
Anyone building, designing or securing web applications

KEY TAKEAWAY
Understand how to take advantage of the underlying security model of the web

ABSTRACT

The web has undergone a dramatic transformation since the first static HTML
documents. However, the underlying security model remains mostly unchanged. Its
flaws have resulted in nefarious security vulnerabilities. But to be fair, the
security model is also the foundation of many modern defenses. And in today's
client-side applications, the web's security model is an essential cornerstone.

In this session, we make this underlying security model explicit. We show that
the Same-Origin Policy is too liberal. As a result, we suffer from attacks such
as Cross-Site Request Forgery, cross-site scripting, and more. We also explore
how you can leverage the security model for better security. You will learn how
to leverage concepts such as domain separation and origin isolation. Overall,
this session offers the foundation for other web security topics here at
SecAppDev.
