
background-image: url(../assets/rust-test-pattern.jpg)

---

class: center, middle

# How Rust helps us make safer and more secure code

---

class: bullets-7x7

# Agenda

1. What is safety
1. Examples of incorrect code and Rust equivalents
1. Multithreading
1. Caveats
1. Case studies

---

class: center, middle

# Who am I?

---

class: center, middle

<img class="full-image" src="../assets/integer32.png" alt="i32 logo" />

???

- Cofounder

---

class: center, middle

# Stack Overflow

<img class="kirby" src="../assets/kirby.png" alt="gravatar" />

???

- \#1 answerer on Stack Overflow for Rust tag

---

class: center, middle

# Rust Playground

<img class="full-image" src="../assets/playground.png" alt="playground screenshot" />

### play.rust-lang.org

???

- maintainer

---

class: bullets-7x7

# Jake Goulding

- Rust infrastructure team
- Working on a Rust video course for Manning
- A handful of crates
- Help out with AVR-Rust

???

- C background
- XML crates, parsing, low-level assembly

---

class: center, middle

# Who are you?

???

- Who is a programmer?
- What languages?
- Are you Pen Tester?
- What languages are most fruitful to test?

---

class: center, middle

# Disclaimer

<video src="./assets/danger-will-robinson.mp4" autoplay="autoplay" loop muted />

---

class: bullets-7x7

# Disclaimer

- Examples of *bad* C and C++ code
- Does not mean *all* C and C++ code is bad
- Does **not** mean C or C++ programmers are bad
- Not a professional security researcher

---

class: center, middle

# What does Rust mean by "safe"?

---

class: bullets-7x7

# What does Rust mean by "safe"?

.center[<img class="rust-homepage" src="assets/rust-homepage.png" alt="Rust Homepage"/>]

???

- guaranteed memory safety
- threads without data races
- Among other bullet points

---

class: center, middle

# Prevention of *undefined behavior*

???

- Has anyone heard of undefined behavior?
- Means the compiler can do *whatever it wants*.

---

# Example

```c
void foo() {
  device_t *dev = NULL;
  int id = dev->id;
  if (dev) {
    // ...
  }
}
```

???

- Normally would segfault
- But it's valid to read memory at address 0.
- But the optimizer knows it's invalid to dereference a NULL
- The optimizer decides to convert this check to a no-op.
- Arbitrary code execution in kernel mode
- https://lwn.net/Articles/342330/
- Kernel turned on a compiler option disabling the optimization.

---

# Rust has no NULL references

This code isn't even possible in Rust.

> I call it my billion-dollar mistake. It was the invention of the
> null reference in 1965. [...] This has led to innumerable errors,
> vulnerabilities, and system crashes, which have probably caused a
> billion dollars of pain and damage in the last forty years.

.right[Tony Hoare, 2009]

???

- https://en.wikipedia.org/wiki/Tony_Hoare

> My goal was to ensure that all use of references should be
> absolutely safe, with checking performed automatically by the
> compiler.

> But I couldn't resist the temptation to put in a null reference,
> simply because it was so easy to implement.

- The problem is that a valid value and an invalid value are stuck in the same type

---

# Rust equivalent

```rust
fn foo() {
    let dev: Option<Device> = None;
    let id = dev.id;
    if dev {
        // ...
    }
}
```

???

- `Option` is an enum that can be `Some` or `None`.

--

<pre class="compiler-error compiler-error--small">
<b>error[E0609]</b>: no field `id` on type `std::option::Option<Device>`
  |
5 |     let id = dev.id;
  |                  ^^
</pre>

???

- Because it's a different type, the compiler can tell us about it

---

# Rust equivalent

```rust
fn foo() {
    let dev: Option<Device> = None;
    if `let Some(dev) = dev` {
        let id = dev.id;
        // ...
    }
}
```

???

- The compiler helps prevent logic errors by making sure we address all variants of enums
- The optimizer actually converts this code to the same as the C
- No runtime penalty performance

---

# Example

```c
bool foo(int x) {
  return x+1 > x;
}
```

--

Equivalent to

```c
bool foo(int x) {
  return true;
}
```

???

- Either true or UB due to signed overflow
- Optimizer decides to always return true

---

# Rust equivalent

```rust
fn foo(x: i32) -> bool {
    x + 1 > x
}

fn main() {
    foo(std::i32::MAX);
}
```

--

#### Debug

<pre class="compiler-error compiler-error--small">
thread 'main' panicked at 'attempt to add with overflow', src/main.rs:2:5
</pre>

--

#### Release

<pre class="output-success">
false
</pre>

---

class: bullets-7x7

# Rust defines certain types of behavior

- Signed overflow
  - abort in debug
  - wrap in release
- Can't shift numbers by a negative amount
- Can't shift numbers by more than number of bits

???

- May change to abort in release if cheap enough
- Benefit of a new language; can make decisions like this

---

# Example

```c
int main() {
  int age = 42;
  age += 1;
  printf("%d\n", age);
}
```

--

<pre class="output-success">
43
</pre>

???

- Just kidding, nothing wrong here

---

# Rust equivalent

```rust
fn main() {
    let age = 42;
    age += 1;
    println!("{}", age);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0384]</b>: cannot assign twice to immutable variable `age`
  |
2 |     let age = 42;
  |         --- first assignment to `age`
3 |     age += 1;
  |     ^^^^^^^^ cannot assign twice to immutable variable
</pre>

---

class: bullets-7x7

# Rust is immutable by default

- Variables changing unexpectedly => hard-to-track bugs
- Unchanging value can be optimizated
- Effect is different from other languages
  - Property of the variable, not the value

???

- `const` in C is only to the current use
  - value can still change from someone else
- `const` in ES6 only means the variable is set once
  - can change the insides as much as you want

---

# Rust mutability is opt-in

```rust
fn main() {
    let `mut` age = 42;
    age += 1;
    println!("{}", age);
}
```

--

<pre class="output-success">
43
</pre>

---

# Example

```c
void foo() {
  struct device_t dev;
  printf("%s\n", dev.name);
}
```

--

<pre class="output-success">
H�}�H��
</pre>

???

- C compilers offer this as a warning, which can be made into an error
- `clang -Wall -Wextra`; needed `--analyze`

---

# Rust equivalent

```rust
fn foo() {
    let dev: Device;
    println!("{}", dev.name);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0381]</b>: use of possibly uninitialized variable: `dev.name`
  |
3 |     println!("{}", dev.name);
  |                    ^^^^^^^^ use of possibly uninitialized `dev.name`
</pre>

???

- This is an error, not a warning

---

# Rust does not allow uninitialized values

---

# Example

```c++
int main() {
  std::vector<int> scores = {1, 2, 3};
  for (auto &i : scores) {
    scores.push_back(i * 2);
    i -= 1;
    std::cout << i << std::endl;
  }
}
```

--

<pre class="output-success">
0
1
2
</pre>

???

- Iterating over references
- Why is that a good thing?
- This "works", but it's really broken.

---

# Example

```c++
int main() {
  std::vector<int> scores = {1, 2, 3};
  for (auto &i : scores) {
    scores.push_back(i * 2);
    i -= 1;
  }
  `for (auto &i : scores) {     `
  ` std::cout << i << std::endl;`
  `}                            `
}
```

???

- If we move the printing to a new loop

--

<pre class="output-success">
1
2
3
2
4
6
</pre>

???

- Can see the values never actually changed

---

class: center, middle

<img class="full-image" src="assets/loop-memory-1.png" alt="vector memory before modification" />

???

- Vector is storing data at offset 0x00

---

class: center, middle

<img class="full-image" src="assets/loop-memory-2.png" alt="vector memory after adding one" />

???

- We add a value, no more room left

---

class: center, middle

<img class="full-image" src="assets/loop-memory-3.png" alt="vector memory after reallocation" />

???

- We allocate new memory, copy the values, free the old memory

---

# Rust equivalent

```rust
fn main() {
    let scores = vec![1, 2, 3];
    for i in &scores {
        scores.push(*i * 2);
        *i -= 1;
        println!("{}", i);
    }
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0596]</b>: cannot borrow immutable local variable `scores` as mutable
  |
2 |     let scores = vec![1, 2, 3];
  |         ------ consider changing this to `mut scores`
3 |     for i in &scores {
4 |         scores.push(*i * 2);
  |         ^^^^^^ cannot borrow mutably

...
</pre>

---

# Rust equivalent

```rust
fn main() {
    let `mut` scores = vec![1, 2, 3];
    for i in &scores {
        scores.push(*i * 2);
        *i -= 1;
        println!("{}", i);
    }
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0594]</b>: cannot assign to immutable borrowed content `*i`
  |
5 |         *i -= 1;
  |         ^^^^^^^ cannot borrow as mutable

...
</pre>

---

# Rust equivalent

```rust
fn main() {
    let mut scores = vec![1, 2, 3];
    for i in &`mut` scores {
        scores.push(*i * 2);
        *i -= 1;
        println!("{}", i);
    }
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0499]</b>: cannot borrow `scores` as mutable more than once at a time
  |
3 |     for i in &mut scores {
  |                   ------ first mutable borrow occurs here
4 |         scores.push(*i * 2);
  |         ^^^^^^ second mutable borrow occurs here
...
7 |     }
  |     - first borrow ends here
</pre>

---

# Rust equivalent

```rust
fn main() {
    let mut scores = vec![1, 2, 3];
    for i in 0..scores.len() {
        let new_val = scores[i] * 2;
        scores.push(new_val);
        scores[i] -= 1;
    }
    println!("{:?}", scores);
}
```

???

- Depends on what you really wanted
- Did you want the infinite loop?
- This version doesn't keep a reference to the item for the entire loop.

--

<pre class="output-success">
[0, 1, 2, 2, 4, 6]
</pre>

---

# Example

```c++
void print_score(int *score) {
  std::cout << *score << std::endl;
  delete score;
}

int main() {
  auto score = new int(42);
  print_score(score);
  std::cout << *score << std::endl;
}
```

???

- Here we allocate an item and pass in a pointer to it
- Unclear who is responsible for freeing it.

--

<pre class="output-success">
42
42
</pre>

???

- This prints values, but is actually accessing invalid memory
- Modern C++ programmers avoid `new`/`delete`, using `std::unique_ptr` instead

---

# Example (part 2)

```c++
void print_score(std::unique_ptr<int> score) {
  std::cout << *score << std::endl;
}

int main() {
  auto score = std::make_unique<int>(42);
  print_score(std::move(score));
  std::cout << *score << std::endl;
}
```

--

<pre class="output-success">
42
</pre>
<pre class="compiler-error compiler-error--small">
Segmentation fault: 11
</pre>

???

- Moving a `std::unique_ptr` is defined as nulling the source
- This segmentation fault is better!

---

# Rust equivalent

```rust
fn print_score(score: Box<i32>) {
    println!("{}", score);
}

fn main() {
    let score = Box::new(42);
    print_score(score);
    println!("{}", score);
}
```

???

--

<pre class="compiler-error compiler-error--small">
<b>error[E0382]</b>: use of moved value: `score`
  |
7 |     print_score(score);
  |                 ----- value moved here
8 |     println!("{}", score);
  |                    ^^^^^ value used here after move
  |
  = note: move occurs because `score` has type `std::boxed::Box<i32>`,
          which does not implement the `Copy` trait
</pre>

---

class: bullets-7x7

# Ownership

- Values are moved by default
- Also called "transferring ownership"
- More efficient
  - Old value doesn't need to be in a valid state
- Owner controls when resources cleaned up (RAII)

???

- Would be super annoying if that's all we had

---

class: bullets-7x7

# Borrowing

- Access value without transferring ownership
- Can be immutable or mutable
- There are rules. Only one of:
  - Many immutable borrows
  - One mutable borrow

???

- Don't have to move bits around
- Sound like a read-write lock to anyone?

---

# Borrowing

```rust
fn print_score(score: `&i32`) {
    println!("{}", score);
}

fn main() {
    let score = Box::new(42);
    print_score(`&score`);
    println!("{}", score);
}
```

--

<pre class="output-success">
42
42
</pre>

???

- Correct C++ solution looks like this too

---

# Mutable borrowing

```rust
fn print_score(score: &i32) {
    `*score += 1;`
    println!("{}", score);
}

fn main() {
    let score = Box::new(42);
    print_score(&score);
    println!("{}", score);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0368]</b>: binary assignment operation `+=` cannot
              be applied to type `&i32`
  |
2 |     score += 1;
  |     -----^^^^^
  |     |
  |     cannot use `+=` on type `&i32`
</pre>

---

# Mutable borrowing

```rust
fn print_score(score: &`mut` i32) {
    *score += 1;
    println!("{}", score);
}

fn main() {
    let `mut` score = Box::new(42);
    print_score(&`mut` score);
    println!("{}", score);
}
```

--

<pre class="output-success">
43
43
</pre>

---

# Example

```c++
int &biggest(int &a, int &b) {
  return (a > b) ? a : b;
}

int main() {
  auto a = std::make_unique<int>(0);
  auto max = *a;

  {
    auto b = std::make_unique<int>(100);
    max = biggest(*a, *b);
  }

  std::cout << max << std::endl;
}
```

--

<pre class="output-success">
100
</pre>

---

#### Rust equivalent

```rust
fn biggest(a: &i32, b: &i32) -> &i32 {
    if a > b { a } else { b }
}

fn main() {
    let a = Box::new(0);
    let max = {
        let b = Box::new(100);
        biggest(&a, &b);
    };
    println!("{}", max);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0106]</b>: missing lifetime specifier
  |
1 | fn biggest(a: &i32, b: &i32) -> &i32 {
  |                                 ^ expected lifetime parameter
  |
  = help: this function's return type contains a borrowed value, but the
          signature does not say whether it is borrowed from `a` or `b`
</pre>

---

class: bullets-7x7

# Lifetimes

- Metadata to relate input references to output references
  - How long the reference will remain valid
- Every reference has an associated lifetime
  - Not always relevant
  - Lifetime elision handles the common cases
- `'static` effectively means "forever"

---

#### Rust equivalent

```rust
fn biggest<`'a`>(a: &`'a` i32, b: &`'a` i32) -> &`'a` i32 {
    if a > b { a } else { b }
}

fn main() {
    let a = Box::new(0);
    let max = {
        let b = Box::new(100);
        biggest(&a, &b)
    };
    println!("{}", max);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0597]</b>: `b` does not live long enough
   |
9  |         biggest(&a, &b)
   |                      - borrow occurs here
10 |     };
   |     ^ `b` dropped here while still borrowed
...
13 | }
   | - borrowed value needs to live until here
</pre>

---

#### Rust equivalent

```rust
fn biggest<'a>(a: &'a i32, b: &'a i32) -> &'a i32 {
    if a > b { a } else { b }
}

fn main() {
    let a = Box::new(0);
    {
        let b = Box::new(100);
        `let max = biggest(&a, &b);`
        println!("{}", max);
    }
}
```

--

<pre class="output-success">
100
</pre>

---

class: center, middle

# Congratulations!

## You just fought the borrow checker

---

.long-quote[
> <b>[heycam]</b> one of the best parts about stylo has been how much easier it has been to implement these style system optimizations that we need, because Rust
>
> <b>[heycam]</b> can you imagine if we needed to implement this all in C++ in the timeframe we have
>
> <b>[heycam]</b> yeah srsly
>
> <b>[bholley]</b> heycam: it’s so rare that we get fuzz bugs in rust code
>
> <b>[bholley]</b> heycam: considering all the complex stuff we’re doing
>
> *heycam remembers getting a bunch of fuzzer bugs from all kinds of style system stuff in gecko*
>
> <b>[bholley]</b> heycam: think about how much time we could save if each one of those annoying compiler errors today was swapped for a fuzz bug tomorrow :-)
>
> <b>[heycam]</b> heh
>
> <b>[njn]</b> you guys sound like an ad for Rust
>
]

???

- Really, the compiler stopped us from doing something bad
- Something that "just works" in C or C++
- A lot of things "just work" in C or C++, until they don't
- Then you have to go debugging
- https://blog.rust-lang.org/2017/11/14/Fearless-Concurrency-In-Firefox-Quantum.html

---

# Example

```c++
int main() {
  std::vector<int> scores = {1, 2, 3};
  auto i = &scores[3];
  std::cout << *i << std::endl;
}
```

???

- One last C example

--

<pre class="output-success">
1073741824
</pre>

---

# Rust equivalent

```rust
fn main() {
    let scores = vec![1, 2, 3];
    let i = &scores[3];
    println!("{}", i);
}
```

--

<pre class="compiler-error compiler-error--small">
thread 'main' panicked at 'index out of bounds: the len is 3 but the
index is 3', /checkout/src/liballoc/vec.rs:1551:10
</pre>

???

- Has to be checked at run time
- Compile time checks require way more advanced languages, or stronger restrictions

---

class: center, middle

# Yeah, but who cares?

---

class: bullets-7x7

# MITRE cares

???

- 96302 entries

--

* "double-free" [42 CVE entries][CVE-DF]

--

* "use-after-free" [321 CVE entries][CVE-UAF]

--

* "uninitialized" [350 CVE entries][CVE-UNINIT]

--

* "null dereference" [1189 CVE entries][CVE-ND]

--

* "out-of-bounds" [1291 CVE entries][CVE-OOB]

[CVE-ND]: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=null+dereference
[CVE-OOB]: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=out-of-bounds
[CVE-UNINIT]: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=uninitialized
[CVE-UAF]: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=use-after-free
[CVE-DF]: https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=double-free

---

class: bullets-7x7

# Mozilla cares

--

- "double-free" 36 bugs found

--

- "out-of-bounds" 84 bugs found

--

- "null dereference" 363 bugs found

--

- "uninitialized" This result was limited to 500 bugs

--

- "use-after-free" This result was limited to 500 bugs

---

class: middle, center

# Multithreading

---

# Multithreading

### The language

- `Send`: can be transferred between threads
- `Sync`: references can be shared between threads

--

### The standard library

- `std::thread`
- `std::sync` (Atomics, Channels, Mutex)

--

### Crate ecosystem

- Futures and async/await

---

# Spawning a thread

```rust
fn main() {
    let mut score = 42;
    std::thread::spawn(|| {
        score += 1;
    });
    println!("{}", score);
}
```

--

<pre class="compiler-error compiler-error--small">
<b>error[E0373]</b>: closure may outlive the current function, but it borrows
              `score`, which is owned by the current function
  |
3 |     std::thread::spawn(|| {
  |                        ^^ may outlive borrowed value `score`
4 |         score += 1;
  |         ----- `score` is borrowed here
help: to force the closure to take ownership of `score` (and any other
      referenced variables), use the `move` keyword
  |
3 |     std::thread::spawn(move || {
  |                        ^^^^^^^
</pre>

---

# `std::thread::spawn`

```rust
pub fn spawn<F, T>(f: F) -> JoinHandle<T>
where
    F: FnOnce() -> T,
    F: Send + 'static,
    T: Send + 'static,
```

???

- Has a trait bound that says any references must live forever
- There are crates that lift this by ensuring the thread exits before references

---

# Multithreading

Crates exist to make parallelization easy and safe.

> you can change a sequential iterator into a parallel iterator just
> by adding the crate, importing the trait and changing `iter` to
> `par_iter`. If it’s not thread-safe to do, then it won’t compile

.right[[Chris Morgan][]]

???

- Many languages have something equivalent, and they all have big warnings

[Chris Morgan]: https://news.ycombinator.com/item?id=16428517

---

class: bullets-7x7

# What isn't related to safety?

- Integer overflow
- Deadlocks
- Leaks of memory and other resources
- Exiting without calling destructors

???

- But doing these is probably not what you want

---

class: middle, center

# Caveats

---

class: bullets-7x7

# Caveats

- The Rust compiler is conservative
- What we think:
  - Prevents code that would cause undefined behavior

--

- What it really is:
  - Prevents code it can't guarantee doesn't cause undefined behavior

???

- That's a fun double negative

---

# The escape hatch

```rust
fn main() {
    let scores = vec![1, 2, 3];
    unsafe {
        let a: *mut i32 = std::ptr::null_mut();
        let b: Device = std::mem::uninitialized();
        let c = &String::new() as *const String;
        let d = scores.get_unchecked(3);
    }
}
```

???

- This disproves all of the points I just said
- This is *unsafe Rust*

---

class: bullets-7x7

# What `unsafe` allows you to do

- Dereference a raw pointer
- Call an unsafe function
- Implement an unsafe trait
- Read or write a mutable static variable
- Read a field of a union

???

- All foreign functions are `unsafe`
- Or write to a field of a union that isn't `Copy`, but that's unstable

---

class: bullets-7x7

# Raw pointers

- References (`&T` / `&mut T`)
  - Are never `NULL`
  - Always point to a valid value
  - Have lifetimes

--

- Raw pointers (`*const T` / `*mut T`)
  - Can be `NULL`
  - Can point anywhere
  - Do not have lifetimes

---

class: bullets-7x7

# Unsafe code builds safe abstractions

- `Vec` uses unsafe code internally
- You don't need unsafe code to use `Vec`
- Type and module systems keep unsafe code contained

---

class: center, middle

# Case studies

---

<h1 class="heartbleed-logo">
  <span>Heartbleed</span>
  <img src="assets/heartbleed.svg" />
</h1>

.center[
<img class="xkcd" src="assets/heartbleed_explanation-1.png" alt="xkcd heartbleed setup" />

https://xkcd.com/1354/
]

---

<h1 class="heartbleed-logo">
  <span>Heartbleed</span>
  <img src="assets/heartbleed.svg" />
</h1>

.center[
<img class="xkcd" src="assets/heartbleed_explanation-2.png" alt="xkcd heartbleed punchline" />

https://xkcd.com/1354/
]

???

- https://en.wikipedia.org/wiki/Cloudbleed

---

class: bullets-7x7

<h1 class="heartbleed-logo">
  <span>Heartbleed root causes</span>
  <img src="assets/heartbleed.svg" />
</h1>

- Buffer overread
- Did not validate user input
- Reusing a buffer
- Wrote their own memory allocator

---

class: center, middle

# Would Rust have helped?

--

## Maybe

???

- Bounds checks
- Reusing a buffer tends to more obviously fail

---

# Gotofail

```c
if ((err = ReadyHash(&SSLHashSHA1, &hashCtx)) != 0)
    goto fail;
if ((err = SSLHashSHA1.update(&hashCtx, &clientRandom)) != 0)
    goto fail;
if ((err = SSLHashSHA1.update(&hashCtx, &serverRandom)) != 0)
    goto fail;
if ((err = SSLHashSHA1.update(&hashCtx, &signedParams)) != 0)
    goto fail;
    `goto fail;`
if ((err = SSLHashSHA1.final(&hashCtx, &hashOut)) != 0)
    goto fail;
```

???

- Unfortunately, `err` was set to `0` which means success
- this function always returned success

---

class: bullets-7x7

# Gotofail root causes

- Copy-pasting
- Poor alignment / curly braces
- `goto` makes it hard to follow code
- Lack of dead code warnings
- Intermingled error and success code

---

# Rust equivalent

```rust
fn verify(SSLHashSHA1: SomeType) -> Result<bool, SslError> {
    let hashCtx = SSLHashSHA1.ReadyHash()?;
    SSLHashSHA1.update(&hashCtx, &clientRandom)?;
    SSLHashSHA1.update(&hashCtx, &serverRandom)?;
    SSLHashSHA1.update(&hashCtx, &signedParams)?;
    let hashOut = SSLHashSHA1.complete(&hashCtx)?;

    // ...
}
```

---

# Rust equivalent

```rust
fn verify(SSLHashSHA1: SomeType) -> `Result<bool, SslError>` {
    let hashCtx = SSLHashSHA1.ReadyHash()`?`;
    SSLHashSHA1.update(&hashCtx, &clientRandom)`?`;
    SSLHashSHA1.update(&hashCtx, &serverRandom)`?`;
    SSLHashSHA1.update(&hashCtx, &signedParams)`?`;
    let hashOut = SSLHashSHA1.complete(&hashCtx)`?`;

    // ...
}
```

???

- We haven't even talked about how Rust can help with logic
- `Result` is a type used to indicate success or failure
- Question mark returns the error early from the function

---

class: center, middle

# Would Rust have helped?

--

## Probably

---

# Stagefright

```c++
uint8_t *buffer = new (std::nothrow)
    uint8_t[size + chunk_size];

if (size > 0) {
    memcpy(buffer, data, size);
}
```

???

- Library to parse media files
- Crafted MMS message could cause code execution
- https://en.wikipedia.org/wiki/Stagefright_(bug)
- https://googleprojectzero.blogspot.be/2015/09/stagefrightened.html

---

# Stagefright

```c++
uint8_t *buffer = new (std::nothrow)
    uint8_t[size + `chunk_size`];

if (size > 0) {
    memcpy(buffer, data, size);
}
```

???

- If size + chunk_size overflows it wraps around
- Buffer will be less than size and violate a variety of assumptions

---

# Stagefright

```c++
if (`SIZE_MAX - chunk_size <= size`) {
    return ERROR_MALFORMED;
}

int8_t *buffer = new (std::nothrow)
    uint8_t[size + chunk_size];

if (size > 0) {
    memcpy(buffer, data, size);
}
```

---

# Stagefright

```c++
//  size_t     uint64_t      size_t
//  |          |             |
if (SIZE_MAX - chunk_size <= size) {
    return ERROR_MALFORMED;
}

int8_t *buffer = new (std::nothrow)
    uint8_t[size + chunk_size];

if (size > 0) {
    memcpy(buffer, data, size);
}
```

???

- size_t is 32-bit

---

class: bullets-7x7

# Stagefright root causes

- Untrusted input
- Integer overflow
- Loose integer operations

???

- MP4 data is inherently untrusted

---

class: center, middle

# Would Rust have helped?

--

## Probably

???

- can turn overflow panicking on
- can't access out of bounds
- can't do math on different sizes without explicit casts
- mozilla/mp4parse-rust
- one of the earliest pieces of Rust in FF

---

class: center, middle

# POODLE
# DROWN
# KRACK

???

- KRACK is from here!
- All protocol issues
- https://en.wikipedia.org/wiki/POODLE
- https://en.wikipedia.org/wiki/DROWN_attack
- https://en.wikipedia.org/wiki/KRACK

---

class: center, middle

# Would Rust have helped?

--

## Highly doubtful

---

class: center, middle

# Rowhammer
# Meltdown
# Spectre

???

- All hardware

---

class: center, middle

# Would Rust have helped?

--

## Highly doubtful

---

# "ctrl-d to log in any user with su"

<img class="full-image" src="assets/redox-su-anyone.png" alt="Redox commit" />

???

- double blank macOS - https://objective-see.com/blog/blog_0x24.html
- https://github.com/redox-os/userutils/commit/02759b4a5a347726e6e81d4ee46a2ade86fd9e1e

---

class: center, middle

# Would Rust have helped?

--

<img class="full-image" src="assets/redox-info.png" alt="Redox info" />

---

# Stagefright postmortem

> The best way to prevent these kinds of attacks is either to use a
> higher level language, which manages memory for you (albeit with
> less performance), or to be very, very, very, very careful when
> coding. **More careful than the entirety of the Android security team,
> for sure.**

- https://xda-developers.com/a-demonstration-of-stagefright-like-mistakes/

???

- Quibble with "less performance"
- The compiler is always watching; it never gets tired

---

class: bullets-7x7

# What does Rust bring to the table?

- Reduces or eliminates entire classes of bugs
- Frequently at compile time
- Minimal to no change in performance
- Can *increase* performance
- Allows us to focus more on the logic of the problem

---

# What does Rust bring to the table?

> Rust lets us make new mistakes by preventing us from making the same
> old mistakes over and over.

.right[Carol Nichols, Rust core team member]

---

class: bullets-7x7

# What didn't we talk about?

- Type system
- Safe defaults
- Error handling

---

class: middle, center

### [@jakegoulding](https://twitter.com/JakeGoulding)

<img class="full-image" src="../assets/integer32.png" alt="i32 logo" />

### [integer32.com](https://integer32.com)
