class: center, middle

# Hands-on introduction to Rust

---

class: bullets-7x7

# Agenda (1/2)

1. Cargo
1. Basics and documentation
1. Iterating
1. Making our own types
1. Strings and user input

---

class: bullets-7x7

# Agenda (2/2)

1. Error handling
1. Modules
1. FFI
1. More?

---

class: center, middle

<img class="full-image" src="../assets/integer32.png" alt="i32 logo" />

???

- Cofounder

---

class: center, middle

# Stack Overflow

<img class="kirby" src="../assets/kirby.png" alt="gravatar" />

???

- \#1 answerer on Stack Overflow for Rust tag

---

class: center, middle

# Rust Playground

<img class="full-image" src="../assets/playground.png" alt="playground screenshot" />

### [play.rust-lang.org](https://play.rust-lang.org)

???

- maintainer

---

class: bullets-7x7

# Jake Goulding

- Rust infrastructure team
- Working on a Rust video course for Manning
- A handful of crates
- Help out with AVR-Rust

???

- C background
- XML crates, parsing, low-level assembly

---

class: center, middle

# Who are you?

???

- Who is a programmer?
- What languages?
- Are you a pen tester?
- What languages are most fruitful to test?

---

class: bullets-7x7

# Cargo

- Package manager
- Build tool
  - Code
  - Tests
  - Docs

---

# Cargo

`cargo new --bin name-of-crate`

```
.
├── Cargo.toml
└── src
    └── main.rs
```

<!-- -->

???

- Can also make reusable libraries
- Have students list the directory

---

# Cargo

`cargo run`

```
.
├── Cargo.lock
└── target
    └── debug
```

???

- Have students list the directory

---

# Hello, world!

```rust
fn main() {
    println!("Hello, world!");
}
```

???

- `fn` keyword
- `main` is entry point for an executable
- parenthesis
- curly braces
- 4 space indent
- println is a macro, don't worry too much about that
- semicolons

---

# Printing values

```rust
fn main() {
    println!("Hello, {}", "alice");
    println!("Hello, {:?}", "bob");
}
```

???

- `{}` prints something intended for an end-user
- `{:?}` prints something for a developer

---

# Comments

```rust
fn main() {
    // println!("Hello, {}", "alice");
    /* println!("Hello, {:?}", "bob"); */
}
```

---

class: bullets-7x7

# API Documentation

- https://doc.rust-lang.org/
  - Click on "Standard Library API Reference"
- `rustup doc`
  - `rustup doc --std`

---

# Functions

```rust
fn the_name(arg1: Type1, arg2: Type2, ...) -> ReturnType {
    statement;
    statement;
    expression
}
```

???

- statement has a semicolon at the end
- expression doesn't
- a block can have many statements, last may be expression
- final expression of a block is the value of the block

--

```rust
fn the_name(arg1: Type1, arg2: Type2, ...) {
    return expression;
}
```

???

- Return type is optional
- Can use `return` to exit from a function early

---

# Variables

```rust
let age = 21;
let is_enabled = true;
let emoji = '🌍';
```

???

- keyword `let`
- every variable has a type
- type is inferred

---

# Variables

```rust
fn main() {
    let age = 21;
    age += 1;
}
```

???

- Try this

---

# Variables are immutable by default

```rust
fn main() {
    let `mut` age = 21;
    age += 1;
}
```

---

class: bullets-7x7

# Types

- `u32`: unsigned 32-bit integer
- `i32`: signed 32-bit integer
- `f64`: floating point number
- `String` and/or `&str`: more on these later
- `bool`: a boolean
- `(T1, T2, ...)`: a tuple

---

class: bullets-7x7

# Type inference / explicit types

- Most of the time, you don't need to specify the type
- You can choose to if it helps you learn

```rust
let name: Type = value;
```

---

# `if`

```rust
if condition {
    // statements + optional expression
} else if another_condition {
    // statements + optional expression
} else {
    // statements + optional expression
}
```

???

- No parenthesis on condition
- Evaluates to value of chosen block

---

# `match`

```rust
match expression {
    pattern_1 => /* expression */,
    pattern_2 => {
        // expressions
    }
    _ => /* expression */,
}
```

???

- can have multiple patterns
- first that matches is applied
- can have single expression or curly braces
- catch-all pattern of `_`

---

class: bullets-7x7

# Exercise

- Create a `fibonacci` function
  - `F(n) = F(n-1) + F(n-2)`
  - `F(0) = 0`
  - `F(1) = 1`
- Print out the result of calling the function with `10`




---

# One answer

```rust
fn fibonacci(a: u32) -> u32 {
    if a == 0 {
        0
    } else if a == 1 {
        1
    } else {
        fibonacci(a - 1) + fibonacci(a - 2)
    }
}

fn main() {
    for i in 1..11 {
        println!("{} -> {}", i, fibonacci(i));
    }
}
```

---

# Another answer

```rust
fn fibonacci(a: u32) -> u32 {
    match a {
        0 => 0,
        1 => 1,
        _ => fibonacci(a - 1) + fibonacci(a - 2),
    }
}

fn main() {
    for i in 0..10 {
        let i = i + 1;
        println!("{} -> {}", i, fibonacci(i));
    }
}
```

---

# Vectors

```rust
let mut scores = vec![100, 90, 85];
scores[0] -= 10;
scores.push(100);
println!("scores: {:?}", scores);
```

???

- Rust has arrays, but they are fairly limited compared to `Vec`

---

# `for`

```rust
for name in expression {
     // use name
}
```

???

- `expression` can be anything that creates an iterator

--

- `0..10`
- `vector.iter()`
- `&vector`

---

# Iterating

```rust
for score in scores {
    println!("score: {}", score);
}
```

???

- What happens if we try to do the loop twice?

--

```rust
for score in &scores {
    println!("score: {}", score);
}
```

---

# Iterators

```rust
for i in 0..10 {
    println!("{}", i);
}
```

???

- Check the docs for Iterator

--

```rust
for i in (0..10).map(|x| x * 2) {
    println!("{}", i);
}
```

--

```rust
for i in (0..10).filter(|y| y % 2 == 0).map(|x| x * 2) {
    println!("{}", i);
}
```

---

# Iterators

```rust
let values: Vec<i32> = (0..10).collect();
```

--

```rust
let values: i32 = (0..10).sum();
```

---

class: bullets-7x7

# Exercise

- Print out the values from 0 (inclusive) to 100 (exclusive)
- That are divisible by 3
- And divisible by 7

--

<hr />

- Instead of printing them out, try adding them up

---

# One answer

```rust
fn main() {
    let iter = (0..100)
        .filter(|y| y % 3 == 0)
        .filter(|y| y % 7 == 0);

    for i in iter {
        println!("{}", i);
    }
}
```

---

# Another answer

```rust
fn main() {
    for i in (0..100).filter(|y| y % 3 == 0 && y % 7 == 0) {
        println!("{}", i);
    }
}
```

---

# Structs

```rust
struct TypeName {
    member_name: MemberType,
}
```

```rust
let val = TypeName {
    member_name: member_value,
};

val.member_name;
```

---

# Enums

```rust
enum TypeName {
    VariantOne,
    VariantTwo(Type1, Type2),
}
```

```rust
let val_1 = TypeName::VariantOne;
let val_2 = TypeName::VariantTwo(some_value);

match val_2 {
    TypeName::VariantOne => println!("one"),
    TypeName::VariantTwo(v) => println!("two {}", v),
}
```

---

class: bullets-7x7

# Exercise

- Create `Fahrenheit` and `Celcius` structs
- Create a function that converts `Fahrenheit` to `Celcius`
- `C = (F - 32.0) / 1.8`

--

<hr />

- Instead of a struct, do it with a single enum

---

# One answer

```rust
struct Farenheit {
    value: f64,
}

struct Celcius {
    value: f64,
}

fn farenheit_to_celcius(temperature: Farenheit) -> Celcius {
    Celcius {
        value: (temperature.value - 32.0) / 1.8,
    }
}

fn main() {
    let f = Farenheit { value: 70.0 };
    let c = farenheit_to_celcius(f);
    println!("is {:?}", c.value);
}
```

???

- derive - Debug, Copy?

---

# Methods

```rust
impl TypeName {
    fn method_1(`self`, arg1: Type1) -> ReturnType {
        // ...
    }
}
```

--

```rust
impl TypeName {
    fn method_2(`&self`, arg1: Type1) -> ReturnType {
        // ...
    }
}
```

---

class: bullets-7x7

# Exercise

- Create a method that converts `Celcius` to `Fahrenheit`
- `F = C * 1.8 + 32.0`

---

# An answer

```rust
struct Farenheit {
    value: f64,
}

struct Celcius {
    value: f64,
}

impl Celcius {
    fn to_farenheit(&self) -> Farenheit {
        Farenheit {
            value: (self.value - 32.0) / 1.8,
        }
    }
}

fn main() {
    let c = Celcius { value: 100.0 };
    let f = c.to_farenheit();
    println!("is {:?}", f.value);
}
```

---

class: bullets-7x7

# Strings

- Rust has two primary string types:
- `String`
  - Owns the data
  - Can be extended or reduced
- `&str`
  - References existing data
  - Cannot change length

---

class: bullets-7x7

# Strings

- Can convert from a `&str` to a `String` via `to_string()`

```rust
"hello".to_string()
```

- Can get a `&str` from a `String` via `as_str()`

```rust
String::new().as_str()
```

???

- Can use `to_string` to convert lots of types to a String, actually

---

class: bullets-7x7

# Exercise

- Create a function that prints a number
- Multiples of three print “Fizz” instead of the number
- Multiples of five print “Buzz” instead of the number
- Multiples of both three and five print “FizzBuzz”
- Call the function with the numbers from 1 to 100
- Change the function to return a string instead of printing

---

# One answer

```rust
fn fizz_buzz(i: u32) -> String {
    match (i % 3 == 0, i % 5 == 0) {
        (true, true) => "FizzBuzz".to_string(),
        (true, false) => "Fizz".to_string(),
        (false, true) => "Buzz".to_string(),
        (false, false) => i.to_string(),
    }
}

fn main() {
    for i in 0..100 {
        println!("{}", fizz_buzz(i + 1));
    }
}
```

---

# Reading user input

```rust
use std::io::prelude::*;
use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)
        .expect("Couldn't read from stdin");
    println!("Read: {:?}", input);
}
```

???

- Warning: the string has the trailing newline from hitting enter!


---

# Parsing strings

```rust
fn main() {
    let a = "42".parse::<i32>().expect("Not an integer");
}
```

???

- Turbofish specifies what target type

---

class: bullets-7x7

# Exercise

- Read user input of a temperature and convert it

--

<hr />

- Ask if it's Celcius or Fahrenheit

---

class: bullets-7x7

# Handling errors

- Rust does not have exceptions
- You can:
  - return an error
  - panic

---

class: bullets-7x7

# Returning errors

- `Result` is an enum

    ```rust
    enum Result<T, E> {
        Ok(T),
        Err(E),
    }
    ```

- Can't currently be used in `main` or in tests

???

- Generic types for success and failure cases

---

class: bullets-7x7

# Chained error returns

The `?` operator is syntax sugar for returning an error or getting the success value.

```rust
fn may_error() -> Result<i32, bool> { /* */ }

fn calls_may_error() -> Result<i32, bool> {
    let val = may_error()?;
    Ok(val * 2)
}
```

---

class: bullets-7x7

# Panicking

- Tears down the current thread
  - If it's the main thread, program exits
- Safe to do, in Rust terms
- When to panic:
  - Great for prototyping and "learning Rust" workshops
  - OK for an executable
  - Not good for a library
      - Unless there's an error from the library writer

---

# Explicit panics

```rust
panic!("This shouldn't happen");
unreachable!("This code can't be run but it is");
unimplemented!("This code hasn't been written yet");
```

---

class: bullets-7x7

# Implicit panics

- `Option::unwrap` / `Result::unwrap`
- `Option::expect("reason")` / `Result::expect("reason")`
- Indexing out of bounds (`foo[1000]`)

---

class: bullets-7x7

# Exercise

- Write a function that adds two `i32` values
- If either of the values are greater than `10`, return an error
- If the sum is greater than `15`, return an error
- Call the function and panic if it fails

## Hints

- Write a helper function for the repeated logic and use `?`
- Use `()` as your returned error type and its value

---

# One answer

```rust
fn acceptable_number(a: i32, max: i32) -> Result<i32, ()> {
    if a > max {
        Err(())
    } else {
        Ok(a)
    }
}

fn strange_adder(a: i32, b: i32) -> Result<i32, ()> {
    let c = acceptable_number(a, 10)? +
            acceptable_number(b, 10)?;
    acceptable_number(c, 15)
}

fn main() {
    strange_adder(10, 10).expect("Oh no");
}
```

---

# Modules

```rust
mod example {
    fn in_module() {
        println!("I'm inside a module");
    }
}

// accessed as example::in_module
```

???

- `mod` keyword
- Probably getting a warning about an unused function? Go ahead and call it.

---

# Visibility

```rust
pub fn call_me() { /* ... */ }

pub struct Monster {
    pub hit_points: u8,
}

pub enum State {
    OnFire,
    Electrified,
}

pub mod example { /* ... */ };
```

???

- `pub` basically means "the thing that contains me can see this"
- There's some other nuanced visibility, but we don't need to worry about it for now

---

# Modules in files

**src/main.rs**

```rust
mod example {
    pub fn in_module() {
        println!("I'm inside a module");
    }
}
```

???

- This seems to be a complicated aspect for many people.

---

# Modules in files

**src/main.rs**

```rust
mod example;
```

**src/example.rs**

```rust
pub fn in_module() {
    println!("I'm inside a module");
}
```

???

- TODO: more nested?

---

# Exercise

Create a function which calls two others. The parent function should be called in `main`

```rust
fn main() {
    println!("{}", secret::code());

    // secret::key() // Has an error!
    // secret::signature() // Has an error!
}
```

---

class: bullets-7x7

# FFI

- Use C code from Rust
- There's a lot of battle-tested code out there

???

- Possible to call Rust code from C as well

---

class: bullets-7x7

# Target library

- Tracks a persons name and age
- Look at `src/foo.h` and `src/foo.c`

---

# Scaffolding

```rust
extern crate libc;

mod ffi {
    use libc;

    pub enum Person {}

    extern "C" {
        pub fn person_new(
            name: *const libc::c_char,
            age: libc::uint8_t
        ) -> *mut Person;
    }
}
```

???

- crates and corresponding use statement
- opaque type
- extern block
- raw pointers

---

# Exercise

Add extern declarations for:

- `person_free`
- `person_name`
- `person_age`
- `person_get`

---

# One answer

```rust
pub fn person_free(p: *mut Person);

pub fn person_name(p: *const Person) -> *const libc::c_char;

pub fn person_age(p: *const Person) -> libc::uint8_t;

pub fn person_get_older(p: *mut Person);
```

---

# C strings

- `String`
  - (capacity, length, data pointer)
  - Owns the data

- `&str`
  - (length, data pointer)
  - Borrows the data

- `char *`
  - (data pointer)
  - Owns **or** borrows the data

???

- NUL-terminated string

---

class: bullets-7x7

# Interoperating with C strings

- `std::ffi::CStr`
  - counterpart to `&str`
  - `to_owned`: convert to `CString`
- `std::ffi::CString`
  - counterpart to `String`
  - `as_ptr`: convert to `&CStr`

???

- Warning on usage of `as_ptr`!

---

class: bullets-7x7

# The `unsafe` keyword

- When defining a function
- When calling unsafe functions
- When defining or implementing traits

```rust
unsafe fn example(a: i32) -> i32 {
    a + 1
}

fn main() {
    unsafe {
        example(41);
    }
}
```

---

class: bullets-7x7

# `unsafe` functions

- The code cannot *statically* guarantee it is safe
- Often based on some choice of arguments
- Sometimes based on pre-existing state

---

class: bullets-7x7

# `unsafe` blocks

- Calling this set of unsafe functions is always safe

---

class: bullets-7x7

# Powers of the `unsafe` keyword

- Dereferencing a raw pointer
- Reading or writing a mutable static variable
- Calling an unsafe function
  - All foreign functions are unsafe
- Implementing an unsafe trait

## Warning

- Not permitted to break any of Rust's guarantees
- Up to programmer to verify, not the compiler

---

class: bullets-7x7

# Exercise

- Create a person via `person_new`
- Print out result of `person_age`
- Optional: clean up memory via `person_free`

## Hints

  - Will use `CString` or `CStr`
  - Will use `unsafe` blocks

---

# One answer

```rust
fn main() {
    let name = CString::new("hello")
        .expect("Invalid C string");

    unsafe {
        let person = ffi::person_new(name.as_ptr(), 21);
        let age = ffi::person_age(person);
        println!("{}", age);
    }
}
```

---

class: bullets-7x7

# Exercise

Create a nicer Rust wrapper struct called `Person`.

```rust
fn main() {
    let mut p = Person::new("Vivian", 21);
    println!("{}", p.name());
    println!("{}", p.age());

    p.get_older();
    println!("{}", p.age());
}
```

---

# Automatically freeing resources

- `Drop` is a trait known to the compiler
- Called when a type goes out of scope

```rust
impl Drop for MyType {
    fn drop(&mut self) { /* ... */ }
}
```

---

class: bullets-7x7

# Exercise

- Convert the wrapper struct to use `Drop`

---

class: bullets-7x7

# Extra ideas

- Traits
- Generics
