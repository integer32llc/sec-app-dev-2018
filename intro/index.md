
background-image: url(../assets/rust-test-pattern.jpg)

---

class: center, middle

# Introduction to Rust

---

class: bullets-7x7

# Goal

Have a good chance of being able to read and understand some Rust code.

---

class: bullets-7x7

# Agenda

1. What's not that unique about Rust
1. What makes Rust somewhat unique
1. What makes Rust really unique
1. Beyond the language
1. Beyond the code
1. Who is using Rust

---

class: center, middle

# Who am I?

---

class: center, middle

<img class="full-image" src="../assets/integer32.png" alt="i32 logo" />

???

- Cofounder

---

class: center, middle

# Stack Overflow

<img class="kirby" src="../assets/kirby.png" alt="gravatar" />

???

- \#1 answerer on Stack Overflow for Rust tag

---

class: center, middle

# Rust Playground

<img class="full-image" src="../assets/playground.png" alt="playground screenshot" />

### [play.rust-lang.org](https://play.rust-lang.org)

???

- maintainer

---

class: bullets-7x7

# Jake Goulding

- Rust infrastructure team
- Working on a Rust video course for Manning
- A handful of crates
- Help out with AVR-Rust

???

- C background
- XML crates, parsing, low-level assembly

---

class: center, middle

# Who are you?

???

- Who is a programmer?
- What languages?
- Are you a pen tester?
- What languages are most fruitful to test?

---

class: center, middle

# What is Rust?

---

# Metal thing

.center[
<img class="rust-chain" src="./assets/rust-chain.jpg" alt="a rusty chain" />
]

???

---

# Fungus

<img class="full-image" src="./assets/rust-fungus.jpg" alt="rust the fungus" />

.right[[John Tann][fungus]]

???

[fungus]: https://www.flickr.com/photos/31031835@N08/6710089489

---

# Video Game

<img class="full-image" src="./assets/rust-game.png" alt="rust the game" />

---

# Back-to-back Stanley Cup winner

.center[
<img class="rust-hockey" src="./assets/rust-hockey.jpg" alt="rust the hockey player" />
]

.right[[Peter Diana/Post-Gazette][rust-hockey]]

???

[rust-hockey]: http://www.post-gazette.com/sports/penguins/2016/09/02/Penguins-Prospectus-Bryan-Rust/stories/201609020105

---

# What is Rust?

> Rust is a systems programming language that runs blazingly fast,
> prevents segfaults, and guarantees thread safety.

???

- Does this mean anything to anyone?

---

class: code-splash, middle

```rust
fn main() {
    println!("Hello, world!");
}
```

???

- 1.0 on May 15, 2015
- Current version is 1.24.0
- Release every six weeks

---

class: center, middle

# What's not that unique about Rust?

???

- "The Language Strangeness Budget"
- Curly braces

---

# Variables

```rust
let age = 21;
let is_enabled = true;
let emoji = '🌍';
```

???

- Don't have to specify the type, it's inferred from the context.

---

class: bullets-7x7

# Built-in primitive types

- Signed integers: `i8`, `i16`, `i32`, `i64`, `i128`
- Unsigned integers: `u8`, `u16`, `u32`, `u64`, `u128`
- Floating point numbers: `f32`, `f64`
- Booleans: `bool`
- Unicode scalar values: `char`
- Fixed-sized collection of one type: `[T; N]` ("array")
- Fixed-size collection of arbitrary types: `(A, B)` ("tuple")

???

- Everything has a well-defined, fixed size
- There's a few others that are a bit more nuanced

---

# Statically, strongly typed

```rust
let a: u8 = 42;
let b: bool = a;
```

<pre class="compiler-error">
error[E0308]: mismatched types
  |
3 | let b: bool = a;
  |               ^ expected bool, found u8
  |
</pre>

---

# Control flow: `if`

```rust
if 1 < 2 {
    42.0
} else if 3 > 4 {
    -32.4
} else {
    99.0
}
```

???

- No need for parenthesis around the condition
- the `if` evaluates to the value of the chosen block

---

# Control flow: `while`

```rust
while 2 > 3 {
    println!("never printed");
}
```

???

- Also `break`, `continue`

---

# Control flow: `for`

```rust
for i in 0..3 {
    println!("{}", i);
}
```

???

- Works on iterators
- Also `break`, `continue`

---

# Control flow: `loop`


```rust
loop {
    println!("I go forever!");
}
```

???

- `break` with a value, `continue`

---

# Functions

```rust
fn is_teenager(age: u8) -> bool {
    age >= 13 && age <= 19
}

fn greeter(name: String, age: u8) {
    let next_age = age + 1;

    println!(
        "Hello, {}! Next year you will be {}.",
        name, next_age
    );

    if is_teenager(next_age) {
        println!("A teenager!");
    }
}
```

???

- keyword `fn`
- zero or more arguments with `name: type`
- optional return type, denoted by the thin arrow
- Implicit returns, just like `if`

---

# Structs

```rust
struct DungeonMonster {
    name: String,
    health: u32,
    damage_per_attack: f32,
}
```

???

- Allow aggregating properties
- still `name: type`

---

# Inherent methods

```rust
impl DungeonMonster {
    fn new() -> DungeonMonster {
        DungeonMonster {
            name: String::from("Grendel"),
            health: 42,
            damage_per_attack: 99.0,
        }
    }

    fn print_status(&self) {
        println!(
            "{} the monster has {} health",
            self.name, self.health
        )
    }
}
```

???

- Methods can be added to a struct
- Can just be associated functions or can make use of the struct's data

---

# Calling methods

```rust
fn main() {
    let monster = DungeonMonster::new();
    monster.print_status();
}
```

???

- Use `::` for the associated functions, like a namespace
- Use a `.` for methods

---

# Traits

```rust
trait Monster {
    fn damage(&self) -> f32;

    fn roar(&self) {
        println!("I roar!");
    }
}

impl Monster for DungeonMonster {
    fn damage(&self) -> f32 {
        self.damage_per_attack
    }
}

fn main() {
    let monster = DungeonMonster::new();
    monster.damage();
    monster.roar();
}
```

???

- Can provide methods
- Methods can have default values
- You can create your own traits and implement them for types you didn't create

---

# Generics

```rust
fn be_noisy<M>(monster: M)
where
    M: Monster,
{
    monster.roar();
}

fn main() {
    let monster = DungeonMonster::new();
    be_noisy(monster)
}
```

???

- Rust allows for *monomorphization* - each used concrete type generates unique code at compile time
- `HashSet<K>` is `HashMap<K, ()>`

---

# Trait objects

```rust
fn be_noisy(monster: Box<Monster>) {
    monster.roar();
}

fn main() {
    let monster = DungeonMonster::new();
    be_noisy(Box::new(monster))
}
```

???

- This is one kind of trait object
- Closer to polymorphism in many languages
- Exact code path is chosen at runtime
- Requires some indirection

---

class: center, middle

# Questions?

---

class: center, middle

# What makes Rust somewhat unique?

???

- I'll mention some languages; these aren't an exhaustive list, just
  ones I think many people will have heard of.

---

# Variables are immutable by default

```rust
let a = 1;
a += 1;
```

<pre class="compiler-error">
error[E0384]: cannot assign twice to immutable variable `a`
  |
2 | let a = 1;
  |     - first assignment to `a`
3 | a += 1;
  | ^^^^^^ cannot assign twice to immutable variable
</pre>

???

- Lots of functional languages do this
- Reduces places to reason about accidental changes
- Ever passed a collection to a function and it modified it?
- Default encourages usage
- Possible for the compiler to make some optimizations

---

# Variables are immutable by default

```rust
let `mut` a = 1;
a += 1;
```

???

- Adding a `mut` keyword lets the compiler know we want to change something

---

# Values not automatically placed on the heap

```rust
let enemies = [DungeonMonster::new(), DungeonMonster::new()];
```

---

# Values not automatically placed on the heap

```rust
let enemies = [DungeonMonster::new(), DungeonMonster::new()];
```

<img class="full-image" src="./assets/array-boxed.png" alt="memory when boxed" />

---

# Values not automatically placed on the heap

```rust
let enemies = [DungeonMonster::new(), DungeonMonster::new()];
```

<img class="full-image" src="./assets/array-unboxed.png" alt="memory when not boxed" />

???

- Sometimes known as "value types"
- Dynamic memory allocation has overhead
- Values on the stack are often much faster
- Java's `Integer` vs `int`
- Reduced memory usage
- Better cache coherency

---

# No garbage collector

```rust
fn do_tough_work() {
    // Allocate a vector of numbers
    let powers = vec![1, 2, 4, 8];
    // Memory is freed when variable goes out of scope
}
```

???

- Allows for very easy FFI use, embedded devices
- RAII ("Resource Acquisition Is Initialization")
- C, C++ Objective-C, Pascal have no GC
- C++ has RAII, other languages have subset via e.g. `using`
- http://wiki.c2.com/?LanguagesWithoutGarbageCollection
- http://wiki.c2.com/?ResourceAcquisitionIsInitialization

---

# No NULL

There is no implicit `NULL`, `null`, `nil`, `None`, `undefined`, etc.

> I call it my billion-dollar mistake. It was the invention of the
> null reference in 1965. [...] This has led to innumerable errors,
> vulnerabilities, and system crashes, which have probably caused a
> billion dollars of pain and damage in the last forty years.

.right[Tony Hoare, 2009]

???

- Haskell, Elm, Swift have something equivalent
- https://stackoverflow.com/questions/28106234/are-there-languages-without-null
- https://en.wikipedia.org/wiki/Tony_Hoare

---

# Enums

```rust
enum Option<T> {
    Some(T),
    None,
}
```

???

- Unlike some languages with something called `enum`, Rust's enums can
  have unique values for each variant
- Programming language folk might call this a "sum type" or an Algebraic Data Type
- Methods can be added to an enum, just like a struct
- Scala, TypeScript, Haskell, Elm, Swift have something equivalent
- https://en.wikipedia.org/wiki/Algebraic_data_type

---

# Pattern matching

```rust
let name = Some("Vivian");
match name {
    Some(n) => println!("Hello, {}", n),
    None    => println!("Who are you?"),
}
```

???

- Usually goes hand-in-hand with enums
- Another type of control flow
- Value of chosen branch is value of entire `match`, like `if`
- Also enables destructuring

--

```rust
let name = None;
if let Some(n) = name {
    println!("Hello, {}", n);
}
```


---

# Error handling

```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

???

- Errors are "just" another type returned from a function
- Nothing special like exceptions

---

# Error handling

```rust
fn can_fail() -> Result<i32, String> { /* ... */ }

fn maybe_increment_1() -> Result<i32, String> {
    let val = can_fail()?;
    Ok(val + 1)
}

fn maybe_increment_2() -> Result<i32, String> {
    can_fail().map(|val| val + 1)
}

fn main() {
    match maybe_increment_1() {
        Ok(val) => println!("It worked: {}", val),
        Err(e) => println!("Something went wrong: {}", e),
    }
}
```

???

- Syntax sugar `?` that checks if a value is an `Err`, returns if so, gets OK value otherwise
- Also many combinator functions that work on `Option` and `Result`
- Ultimately you need to handle it, usually by exiting the program

---

# Error handling

```rust
fn can_fail() -> Result<i32, String> { /* ... */ }

fn main() {
    can_fail();
}
```

<pre class="compiler-error">
warning: unused `std::result::Result` which must be used
  |
4 |     can_fail();
  |     ^^^^^^^^^^^
  |
</pre>

???

- Warned if you call something that could fail without explcitly handling it
- Also used to prevent mistakes when using "lazy" types

---

# Iterators

```rust
pub trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;

    // Many useful methods provided
}
```

???

- Iterators are lazy, no work unless advanced
- Only move forward
- Many methods provided for every iterator

---

# Iterators

```rust
let iter_a = 0..3;
let iter_b = [-10, 0, 42].iter();
let iter_c = "hello, world".chars();

iter_a.max();
iter_b.min();

for c in iter_c {
    println!("{}", c);
}
```

???

- Many ways of generating iterators
- Iterators work with `for` loops
- Many `Iterator` methods return new iterators

---

# Closures

```rust
fn sum_of_squares(start: i32, end: i32) -> i32 {
    (start..end)
        .map(|x| x * x)
        .sum()
}
```

???

- Closures are used to parameterize small bits of code
- They can capture their environment
- Reiterate iterators are lazy

---

# Macros

```rust
macro_rules! add_3 {
    ($a:expr, $b:expr, $c:expr) => {
        ($a + $b + $c)
    }
}

fn main() {
    println!("{}", add_3!(1, 2, 3));
}
```

???

- Seen `println` and `vec` macros
- Unlike some languages, these are hygenic
- Can have repetition
- Has some limitations

---

class: center, middle

# Questions?

---

class: center, middle

# What makes Rust really unique?

---

# Ownership

```rust
fn do_tough_work() {
    let mut powers = vec![1, 2, 4, 8];
}
```

???

- Owner controls mutability
- When owner goes out of scope, it's cleaned up

# Borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

---

# Borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = `&`knowledge;
```

???

- All thanks to the humble ampersand
- `knowledge` is something we don't want to duplicate needlessly
- Maybe it takes a lot of memory?
- `a_reference_to_knowledge` points to the same memory
- It's not a copy, so it's very lightweight

---

# Ownership and borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

<img class="full-image" src="./assets/value.png" alt="value analogy" />

---

# Ownership and borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

<img class="full-image" src="./assets/reference.png" alt="reference analogy" />

---

# Immutable and mutable borrowing

```rust
let a_book = String::new();
let reader = `&`a_book;
```

```rust
let `mut` a_book = String::new();
let author = `&mut` a_book;
```

---

# Immutable and mutable borrowing

```rust
let mut a_book = String::new();
let author = &mut a_book;
let reader = &a_book;
```

<pre class="compiler-error">
error[E0502]: cannot borrow `a_book` as immutable because it
              is also borrowed as mutable
  |
3 | let author = &mut a_book;
  |                   ------ mutable borrow occurs here
4 | let reader = &a_book;
  |               ^^^^^^ immutable borrow occurs here
5 | }
  | - mutable borrow ends here
</pre>

???

- If you have a mutable reference, no other references of any kind
- If you have immutable references, as many as you want
- Definitely cannot have both at the same time

---

# Slices

```rust
let scores = vec![1, 2, 3];
let some_scores = &scores[1..];
```

???

- Special kind of reference

---

# Slices

<img class="full-image" src="./assets/vec.png" alt="memory of Vec" />

---

# Slices

<img class="full-image" src="./assets/slice.png" alt="memory of slice" />


???

- Points to the start of some data and knows how many items there are
- Not *that* unique, but the usability and safety comes from borrowing

---

# String slices

```rust
let novel = "hello, world!;
let chapter_1 = &novel[..5];
```

???

- Special kind of slice
- Always guaranteed to be UTF-8

---

# String slices

<img class="full-image" src="./assets/string.png" alt="memory of String" />

---

# String slices

<img class="full-image" src="./assets/string-slice.png" alt="memory of &str" />

---

# The borrow checker

```rust
let a_reference_to_the_book = {
    let a_book = String::new();
    &a_book
};
```

<pre class="compiler-error">
error[E0597]: `a_book` does not live long enough
  |
4 |     &a_book
  |      ^^^^^^ borrowed value does not live long enough
5 | };
  | - `a_book` dropped here while still borrowed
6 |
7 | }
  | - borrowed value needs to live until here
</pre>

???

- We take a reference to the same book, but it is destroyed before we use it
- This is caught at compile time
- No dangling pointers (and all the security holes)

---

# Move semantics

```rust
let a_book = String::new();
let ref_to_the_book = &a_book;
let a_moved_book = a_book;
```

<pre class="compiler-error">
error[E0505]: cannot move out of `a_book` because
              it is borrowed
  |
3 | let ref_to_the_book = &a_book;
  |                        ------ borrow of `a_book`
  |                               occurs here
4 | let a_moved_book = a_book;
  |     ^^^^^^^^^^^^ move out of `a_book` occurs here
</pre>

???

- The previous example (being dropped) was a special case of a move
- An upcoming change to the language will allow this because this specific code is safe

---

# Lifetimes

```rust
fn a_chapter_of_the_book<`'a`>(book: &`'a` str) -> &`'a` str {
    &book[100..]
}

let an_entire_book = String::new();
let a_chapter = a_chapter_of_the_book(&an_entire_book);
```

???

- Lifetimes go hand-in-hand with borrow checker
- Generic lifetimes let us connect input and output references
- These lifetimes are superfluous, just for demonstration

---

class: center, middle

# Beyond the language

---

class: bullets-7x7

# Standard library
## Collections

- Sequences: `Vec`, `VecDeque`, `LinkedList`
- Maps: `HashMap`, `BTreeMap`
- Sets: `HashSet`, `BTreeSet`
- Misc: `BinaryHeap`

---

class: bullets-7x7

# Standard library
## Building blocks

- `Box`
- `String`
- `Option<T>`
- `Result<T, E>`
- `Iterator`

---

class: bullets-7x7

# Standard library
## Platform abstractions and I/O

- Files
- TCP
- UDP
- Threading
- Shared memory primitives
- Subprocesses
- Environment

---

class: bullets-7x7

# Cargo

A build and dependency managment tool in one.

- **new**
- **build**
- **run**
- **test**
- **doc**
- **publish**

???

- Extendable

---

# Crates

<img class="full-image" src="./assets/crates-io.png" alt="crates.io" />

???

- Rust's reusable code packages
- Available from before Rust 1.0
- crates.io
- many common things (e.g. random number generation)
- but not all of them; can still contribute!

---

# Testing

Built-in basic testing framework and assertions

```rust
#[test]
fn addition_works() {
    assert_eq!(2, 1 + 1);
}
```

???

- `#[test]` marks a function as a test
- `cargo test` runs all the tests
- Testing has been fairly predominant in the crates I've used

---

class: bullets-7x7

# Fuzzing

- Provide invalid, unexpected, or random data as input
- Two main implementations
  - cargo-fuzz
  - afl.rs

```rust
fuzz_target!(|data: &[u8]| {
    if let Ok(s) = std::str::from_utf8(data) {
        let _ = fuzzy_pickles::parse_rust_file(s);
    }
});
```

???

- cargo-fuzz uses LLVM
- afl.rs uses American Fuzzy Lop

---

class: bullets-7x7

# Property-based testing

- Generates data structures based on properties
- Shrinking reduces found test cases to manageable size

---

# Property-based testing: QuickCheck

```rust
fn reverse<T: Clone>(xs: &[T]) -> Vec<T> { /* ... */ }
```

```rust
quickcheck! {
    fn quickcheck_example(xs: Vec<u32>) -> bool {
        xs == reverse(&reverse(&xs))
    }
}
```

???

- Macro to hide some implementation detail

---

# Property-based testing: Proptest

```rust
fn reverse<T: Clone>(xs: &[T]) -> Vec<T> { /* ... */ }
```

```rust
use proptest::collection::vec;
use proptest::num::u32;

proptest! {
    #[test]
    fn proptest_example(ref xs in vec(u32::ANY, 0..100)) {
        xs == &reverse(&reverse(xs))
    }
}
```

???

- Macro to hide some implementation detail

---

# Documentation

```rust
/// A nasty enemy that lives in enclosed spaces
struct DungeonMonster { /* ... */ }

impl DungeonMonster {
    /// Dumps the current HP and name to standard out
    fn print_status(&self) { /* ... */ }
}
```

???

- Built-in, just use three slashes instead of two
- Generate with `cargo doc`
- Can have examples that are built and run
- Community site "docs.rs" automatically builds docs for published crates

--

.center[
<img class="rustdoc" src="./assets/rustdoc.png" alt="rustdoc output" />
]

---

# Developer Tools: rustfmt

```rust
fn main( )
{
  std::iter::repeat(1).take(4).map(|x| x * x).map(|x| x + 4).fold(0, |a, e | { a + e});
    println! ( "Hello, world!" );

}
```

--

```rust
fn main() {
    std::iter::repeat(1)
        .take(4)
        .map(|x| x * x)
        .map(|x| x + 4)
        .fold(0, |a, e| a + e);
    println!("Hello, world!");
}
```

???

- Although it seems silly, this can also protect against bugs
- A consistent style allows you to dive into new code easily

---

# Developer Tools: clippy

```rust
fn switcheroo(scores: &mut[i32], a: usize, b: usize) {
    let tmp = scores[a];
    scores[a] = scores[b];
    scores[b] = tmp;
}
```

--

<pre class="compiler-error">
warning: this looks like you are swapping elements of
         `scores` manually
  |
2 | /     let tmp = scores[a];
3 | |     scores[a] = scores[b];
4 | |     scores[b] = tmp;
  | |___________________^ help: try: `scores.swap(a, b)`
  |
  = note: #[warn(manual_swap)] on by default
</pre>

???

- Lints beyond what the compiler performs
- Some lints might have more false positives than the compiler's
- Over 200 lints created based on real-world experience

---

class: center, middle

# Beyond the code

---

class: bullets-7x7

# Community

- IRC
- Forums
- Reddit
- Stack Overflow
- Meetups
- Conferences

???

- Stack Overflow's most loved language 2x years in a row

---

class: bullets-7x7

# Code of Conduct

- Want the community to be welcoming to all
- Present since day one
- Enforced by moderators

---

class: bullets-7x7

# Requests For Comments (RFCs)

Community-driven process for "substantial" changes to Rust

- Summary
- Motivation
- Guide-level explanation
- Reference-level explanation
- Drawbacks
- Rationale and alternatives
- Unresolved questions

???

- A focus on teaching the proposed change to a new user
- A focus on implementation and cross-interactions

---

# Evolution of error handling

```rust
fn can_fail() -> Result<i32, String> { /* ... */ }

fn maybe_increment() -> Result<i32, String> {
    let val = match can_fail() {
        Ok(v) => v,
        Err(e) => return Err(e),
    };
    Ok(val + 1)
}
```

???

- Originally explicit `match`

---

# Evolution of error handling

```rust
fn can_fail() -> Result<i32, String> { /* ... */ }

fn maybe_increment() -> Result<i32, String> {
    let val = `try!(`can_fail()`)`;
    Ok(val + 1)
}
```

???

- Then the `try` macro

---

# Evolution of error handling

```rust
fn can_fail() -> Result<i32, String> { /* ... */ }

fn maybe_increment() -> Result<i32, String> {
    let val = can_fail()`?`;
    Ok(val + 1)
}
```

???

- Then the `?` operator, stabilized in Rust 1.13 (1.5 years after Rust 1.0)
- Always a possibility for future change
- https://github.com/rust-lang/rfcs/blob/master/text/0243-trait-based-exception-handling.md
- https://blog.rust-lang.org/2016/11/10/Rust-1.13.html

---

class: center, middle

# Who is using Rust?

---

name: friends-list

# Lots of people

<video id="friends-list-video" class="full-image" src="./assets/friends.mp4" muted />

???

- Dropbox, NPM, Oracle and Microsoft
- Not as many as every other language

---

# Who is using Rust?

.center[
<img class="firefox-logo" src="./assets/firefox.svg" alt="firefox logo" />
]

???

- "twice as fast as Firefox from 6 months ago"
- "less memory"

---

# Who is using Rust?

.center[
<img class="chucklefish-logo" src="./assets/chucklefish.png" alt="chucklefish logo" />
]

???

- Best known for publishing Stardew Valley, developing Starbound
- Nontrivial Rust project working on all three major consoles
- "Rust is one of the few languages that really gives you a large
  amount of confidence that your parallel / concurrent code is
  anywhere near correct"

---

class: bullets-7x7

# What's wrong with Rust?

- Slow compile times
- Doesn't support every platform C does
- Steeper learning curve
- IDE support still rudimentary
- Not a library for everything yet
- Not as many jobs as other languages

---

class: bullets-7x7

# What doesn't Rust prevent?

- Deadlocks
- Non-data race conditions
- Leaking memory
- Failing to call destructors
- Crashing the program
- **Logic bugs**

---

class: bullets-7x7

# Where do I go next?

- https://doc.rust-lang.org/
- IRC
- Session on how Rust helps with security
- Rust workshop

---

class: bullets-7x7

# What didn't we talk about?

- Concurrency
 - Threading
 - Asynchronous
- C / FFI
- Embedded
- WebAssembly
- Nightly features
- Unsafe Rust

---

class: middle, center

### [@jakegoulding](https://twitter.com/JakeGoulding)

<img class="full-image" src="../assets/integer32.png" alt="i32 logo" />

### [integer32.com](https://integer32.com)
